import React, { Component } from 'react'
import { Animated, Dimensions, Easing, PanResponder, StyleSheet, TouchableOpacity, View, Text, Image, LogBox } from 'react-native'
LogBox.ignoreLogs(['Each']);
const PropTypes = require('prop-types')
const { width, height } = Dimensions.get('window')
import _ from 'lodash';

const defaultZIndex = 8
const touchZIndex = 99

const USE_NATIVE_DRIVER = false;

export default class FastScroll extends Component {

    constructor(props) {
        super(props)

        this.sortRefs = new Map()

        const itemWidth = props.childrenWidth
        const itemHeight = props.childrenHeight

        this.animPress = new Animated.Value(0);
        this.contentOpacity = new Animated.Value(0);

        // this.reComplexDataSource(true,props) // react < 16.3
        // react > 16.3 Fiber
        const rowNum = 1;
        const dataSource = props.dataSource.map((item, index) => {
            const newData = {}
            const left = width - itemWidth
            const top = parseInt((index / rowNum)) * itemHeight

            newData.data = item
            newData.originIndex = index
            newData.originLeft = left
            newData.originTop = top
            newData.position = new Animated.ValueXY({
                x: parseInt(left + 0.5),
                y: parseInt(top + 0.5),
            })
            newData.trackPosition = new Animated.ValueXY({
                x: parseInt(left + 0.5),
                y: parseInt(top + 0.5),
            })

            newData.scaleValue = new Animated.Value(1)
            newData.right = new Animated.Value(0)
            return newData
        });

        this.state = {
            dataSource: dataSource,
            curPropsDataSource: props.dataSource,
            height: Math.ceil(dataSource.length / rowNum) * itemHeight,
            itemWidth,
            itemHeight,
            moveToIndex: null
        };


        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => {
                this.isMovePanResponder = false
                return false
            },
            onMoveShouldSetPanResponder: (evt, gestureState) => this.isMovePanResponder,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => this.isMovePanResponder,

            onPanResponderGrant: (evt, gestureState) => { },
            onPanResponderMove: (evt, gestureState) => this.moveTouch(evt, gestureState),
            onPanResponderRelease: (evt, gestureState) => this.endTouch(evt),

            onPanResponderTerminationRequest: (evt, gestureState) => false,
            onShouldBlockNativeResponder: (evt, gestureState) => false,
        })
    }

    // react < 16.3
    // componentWillReceiveProps(nextProps) {
    //     if (this.props.dataSource != nextProps.dataSource) {
    //         this.reComplexDataSource(false,nextProps)
    //     }
    // }

    // react > 16.3 Fiber
    static getDerivedStateFromProps(nextprops, prevState) {
        const itemWidth = nextprops.childrenWidth
        const itemHeight = nextprops.childrenHeight
        if (nextprops.dataSource != prevState.curPropsDataSource || itemWidth !== prevState.itemWidth || itemHeight !== prevState.itemHeight) {

            const rowNum = 1;
            const dataSource = nextprops.dataSource.map((item, index) => {
                const newData = {};
                const left = width - itemWidth;
                const top = parseInt(index / rowNum) * itemHeight;

                newData.data = item;
                newData.originIndex = index;
                newData.originLeft = left;
                newData.originTop = top;
                newData.position = new Animated.ValueXY({
                    x: parseInt(left + 0.5),
                    y: parseInt(top + 0.5),
                });
                newData.trackPosition = new Animated.ValueXY({
                    x: parseInt(left + 0.5),
                    y: parseInt(top + 0.5),
                })
                newData.scaleValue = new Animated.Value(1);
                newData.right = new Animated.Value(0)
                return newData;
            });
            return {
                dataSource: dataSource,
                curPropsDataSource: nextprops.dataSource,
                height: Math.ceil(dataSource.length / rowNum) * itemHeight,
                itemWidth,
                itemHeight,
            }
        }
        return null;
    }

    startTouch(touchIndex) {

        //防止拖动
        const fixedItems = this.props.fixedItems;
        if (fixedItems.length > 0 && fixedItems.includes(touchIndex)) {
            return;
        }

        this.isHasMove = false

        if (!this.props.sortable) return

        const key = this._getKey(touchIndex);
        if (this.sortRefs.has(key)) {
            if (this.props.onDragStart) {
                this.props.onDragStart(touchIndex)
            }
            Animated.timing(
                this.state.dataSource[touchIndex].scaleValue,
                {
                    toValue: this.props.maxScale,
                    duration: this.props.scaleDuration,
                    useNativeDriver: USE_NATIVE_DRIVER,
                }
            ).start(() => {
                this.touchCurItem = {
                    ref: this.sortRefs.get(key),
                    index: touchIndex,
                    originLeft: this.state.dataSource[touchIndex].originLeft,
                    originTop: this.state.dataSource[touchIndex].originTop,
                    moveToIndex: touchIndex,
                }
                this.isMovePanResponder = true

                this.setState({ moveToIndex: touchIndex }, () => {
                    Animated.timing(this.contentOpacity, {
                        toValue: 1,
                        duration: this.props.scaleDuration,
                        useNativeDriver: USE_NATIVE_DRIVER
                    }).start();
                })

                this.state.dataSource.forEach((item, index) => {

                    item.position.setValue({
                        x: parseInt(0),
                        y: parseInt(item.originTop + 0.5),
                    })

                    item.trackPosition.setValue({
                        x: parseInt(width - this.props.childrenWidth * 2),
                        y: parseInt(item.originTop + 0.5),
                    })

                    Animated.timing(this.animPress, {
                        toValue: 1,
                        duration: 100,
                        useNativeDriver: USE_NATIVE_DRIVER
                    }).start();
                })
            })
        }
    }



    moveTouch(nativeEvent, gestureState) {

        this.isHasMove = true

        //if (this.isScaleRecovery) clearTimeout(this.isScaleRecovery)

        if (this.touchCurItem) {

            let dx = gestureState.dx
            let dy = gestureState.dy
            const itemWidth = this.state.itemWidth;
            const itemHeight = this.state.itemHeight;

            const rowNum = 1;
            const maxWidth = this.props.parentWidth - itemWidth
            const maxHeight = itemHeight * Math.ceil(this.state.dataSource.length / rowNum) - itemHeight

            let left = 0
            let top = this.touchCurItem.originTop + dy

            let maxTop = this.props.childrenHeight * (this.state.dataSource.length - 1)

            if (top < 0) {
                top = 0
            }

            if (top > maxTop) {
                top = maxTop
            }

            if (this.touchCurItem.ref) {
                this.touchCurItem.ref.setNativeProps({
                    style: {
                        zIndex: touchZIndex,
                    }
                })
            }


            this.state.dataSource[this.touchCurItem.index].position.setValue({
                x: left,
                y: top,
            })

            this.state.dataSource[this.touchCurItem.index].trackPosition.setValue({
                x: parseInt(width - this.props.childrenWidth * 2),
                y: top,
            })

            let moveToIndex = 0
            let moveXNum = dx / itemWidth
            let moveYNum = dy / itemHeight
            if (moveXNum > 0) {
                moveXNum = parseInt(moveXNum + 0.5)
            } else if (moveXNum < 0) {
                moveXNum = parseInt(moveXNum - 0.5)
            }
            if (moveYNum > 0) {
                moveYNum = parseInt(moveYNum + 0.5)
            } else if (moveYNum < 0) {
                moveYNum = parseInt(moveYNum - 0.5)
            }

            moveToIndex = this.touchCurItem.index + moveXNum + moveYNum * rowNum

            if (moveToIndex > this.state.dataSource.length - 1) {
                moveToIndex = this.state.dataSource.length - 1
            } else if (moveToIndex < 0) {
                moveToIndex = 0;
            }

            if (this.props.onDragging) {
                this.props.onDragging(gestureState, left, top, moveToIndex)
            }

            if (this.touchCurItem.moveToIndex != moveToIndex) {
                this.setState({ moveToIndex: moveToIndex })
                const fixedItems = this.props.fixedItems;
                if (fixedItems.length > 0 && fixedItems.includes(moveToIndex)) return;
                this.touchCurItem.moveToIndex = moveToIndex
                this.state.dataSource.forEach((item, index) => {

                    let nextItem = null
                    if (index > this.touchCurItem.index && index <= moveToIndex) {
                        nextItem = this.state.dataSource[index - 1]

                    } else if (index >= moveToIndex && index < this.touchCurItem.index) {
                        nextItem = this.state.dataSource[index + 1]

                    } else if (index != this.touchCurItem.index &&
                        (item.position.x._value != item.originLeft ||
                            item.position.y._value != item.originTop)) {
                        nextItem = this.state.dataSource[index]

                    } else if ((this.touchCurItem.index - moveToIndex > 0 && moveToIndex == index + 1) ||
                        (this.touchCurItem.index - moveToIndex < 0 && moveToIndex == index - 1)) {
                        nextItem = this.state.dataSource[index]
                    }

                    if (nextItem != null) {
                        Animated.timing(
                            item.position,
                            {
                                toValue: { x: parseInt(nextItem.originLeft + 0.5), y: parseInt(nextItem.originTop + 0.5) },
                                duration: this.props.slideDuration,
                                easing: Easing.out(Easing.quad),
                                useNativeDriver: USE_NATIVE_DRIVER,
                            }
                        ).start()
                    }

                })
            }

        }
    }

    endTouch(nativeEvent) {

        Animated.timing(this.contentOpacity, {
            toValue: 0,
            duration: this.props.scaleDuration,
            useNativeDriver: USE_NATIVE_DRIVER
        }).start(() => {
            this.setState({ moveToIndex: null })
        });
        //clear
        if (this.touchCurItem) {
            if (this.props.onDragEnd) {
                this.props.onDragEnd(this.touchCurItem.index, this.touchCurItem.moveToIndex)
            }
            //this.state.dataSource[this.touchCurItem.index].scaleValue.setValue(1)
            Animated.timing(
                this.state.dataSource[this.touchCurItem.index].scaleValue,
                {
                    toValue: 1,
                    duration: this.props.scaleDuration,
                    useNativeDriver: USE_NATIVE_DRIVER,
                }
            ).start(() => {
                if (this.touchCurItem.ref) {
                    this.touchCurItem.ref.setNativeProps({
                        style: {
                            zIndex: defaultZIndex,
                        }
                    })
                }
                this.changePosition(this.touchCurItem.index, this.touchCurItem.moveToIndex)
                this.touchCurItem = null

                Animated.timing(this.animPress, {
                    toValue: 0,
                    duration: 100,
                    useNativeDriver: USE_NATIVE_DRIVER
                }).start();
            })

        }
    }

    onPressOut() {
        this.isScaleRecovery = setTimeout(() => {
            if (this.isMovePanResponder && !this.isHasMove) {
                this.endTouch()
            }
        }, 220)
    }

    changePosition(startIndex, endIndex) {

        if (startIndex == endIndex) {
            const curItem = this.state.dataSource[startIndex]
            if (curItem != null) {
                curItem.position.setValue({
                    x: parseInt(curItem.originLeft + 0.5),
                    y: parseInt(curItem.originTop + 0.5),
                })
                curItem.trackPosition.setValue({
                    x: parseInt(curItem.originLeft + 0.5),
                    y: parseInt(curItem.originTop + 0.5),
                })
            }
            return;
        }

        let isCommon = true
        if (startIndex > endIndex) {
            isCommon = false
            let tempIndex = startIndex
            startIndex = endIndex
            endIndex = tempIndex
        }

        const newDataSource = [...this.state.dataSource].map((item, index) => {
            let newIndex = null
            if (isCommon) {
                if (endIndex > index && index >= startIndex) {
                    newIndex = index + 1
                } else if (endIndex == index) {
                    newIndex = startIndex
                }
            } else {
                if (endIndex >= index && index > startIndex) {
                    newIndex = index - 1
                } else if (startIndex == index) {
                    newIndex = endIndex
                }
            }

            if (newIndex != null) {
                const newItem = { ...this.state.dataSource[newIndex] }
                newItem.originLeft = item.originLeft
                newItem.originTop = item.originTop
                newItem.position = new Animated.ValueXY({
                    x: parseInt(item.originLeft + 0.5),
                    y: parseInt(item.originTop + 0.5),
                })
                item = newItem
            }

            return item
        })

        this.setState({
            dataSource: newDataSource
        }, () => {
            if (this.props.onDataChange) {
                this.props.onDataChange(this.getOriginalData())
            }
            // Prevent RN from drawing the beginning and end
            const startItem = this.state.dataSource[startIndex]
            this.state.dataSource[startIndex].position.setValue({
                x: parseInt(startItem.originLeft + 0.5),
                y: parseInt(startItem.originTop + 0.5),
            })
            const endItem = this.state.dataSource[endIndex]
            this.state.dataSource[endIndex].position.setValue({
                x: parseInt(endItem.originLeft + 0.5),
                y: parseInt(endItem.originTop + 0.5),
            })

            this.state.dataSource[startIndex].trackPosition.setValue({
                x: parseInt(startItem.originLeft + 0.5),
                y: parseInt(startItem.originTop + 0.5),
            })
            this.state.dataSource[endIndex].trackPosition.setValue({
                x: parseInt(endItem.originLeft + 0.5),
                y: parseInt(endItem.originTop + 0.5),
            })
        })

    }

    reComplexDataSource(isInit, props) {
        const itemWidth = this.state.itemWidth;
        const itemHeight = this.state.itemHeight;
        const rowNum = 1;
        const dataSource = props.dataSource.map((item, index) => {
            const newData = {}
            const left = width - itemWidth
            const top = parseInt((index / rowNum)) * itemHeight

            newData.data = item
            newData.originIndex = index
            newData.originLeft = left
            newData.originTop = top
            newData.position = new Animated.ValueXY({
                x: parseInt(left + 0.5),
                y: parseInt(top + 0.5),
            })
            newData.trackPosition = new Animated.ValueXY({
                x: parseInt(left + 0.5),
                y: parseInt(top + 0.5),
            })
            newData.scaleValue = new Animated.Value(1)
            newData.right = new Animated.Value(0)
            return newData
        })

        if (isInit) {
            this.state = {
                dataSource: dataSource,
                height: Math.ceil(dataSource.length / rowNum) * itemHeight
            }
        } else {
            this.setState({
                dataSource: dataSource,
                height: Math.ceil(dataSource.length / rowNum) * itemHeight
            })
        }

    }

    getOriginalData() {
        return this.state.dataSource.map((item, index) => item.data)
    }

    render() {
        let item = _.find(this.state.dataSource, { originIndex: this.state.moveToIndex })
        return (
            <>
                <View style={{ position: 'absolute', top: 0, right: 0, left: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    {
                        item
                        &&
                        <Animated.View style={{ opacity: this.contentOpacity, alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={{ width: width * 0.6, height: width * 0.6 }} source={{ uri: item.data.img }} />
                            <Text style={{ marginTop: 10, fontSize: 20, fontWeight: '800' }}>{item.data.title}</Text>
                            <Text style={{ marginTop: 10, fontSize: 15, fontWeight: '500', fontStyle: 'italic', width: width * 0.5, textAlign: 'center' }}>{item.data.subtitle}</Text>
                        </Animated.View>
                    }
                </View>
                <View
                    style={[styles.container, {
                        width: this.props.parentWidth,
                        height: this.state.height,
                    }]}
                >
                    {this._renderItemView()}
                </View>
            </>
        )
    }

    _getKey = (index) => {
        const item = this.state.dataSource[index];
        return this.props.keyExtractor ? this.props.keyExtractor(item.data, index) : item.originIndex;
    }

    _renderItemView = () => {
        const { maxScale, minOpacity } = this.props
        const inputRange = maxScale >= 1 ? [1, maxScale] : [maxScale, 1]
        const outputRange = maxScale >= 1 ? [1, minOpacity] : [minOpacity, 1]
        const trackWidth = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.childrenWidth, this.props.childrenWidth * 1.6]
        });
        const trackColor = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: ['#cccccc', '#aaaaaa']
        });
        return this.state.dataSource.map((item, index) => {
            const transformObj = {}
            const key = this._getKey(index);

            return (
                <>
                    <Animated.View
                        ref={(ref) => this.sortRefs.set(key, ref)}
                        {...this._panResponder.panHandlers}
                        style={[styles.item, {
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                            left: item.position.x,
                            top: item.position.y,
                            right: item.right,
                        }]}>
                        <TouchableOpacity
                            activeOpacity={1}
                            delayLongPress={this.props.delayLongPress}
                            onPressOut={() => this.onPressOut()}
                            onLongPress={() => this.startTouch(index)}
                        >
                            {
                                item.originIndex == 0
                                &&
                                this.renderItem(item.data, index)
                            }

                        </TouchableOpacity>
                    </Animated.View>
                    <Animated.View style={{
                        position: 'absolute', right: 0, top: item.originTop, height: this.props.childrenHeight, width: trackWidth, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Animated.View style={{ width: 12, height: 12, borderRadius: 6, backgroundColor: trackColor }}></Animated.View>
                    </Animated.View>
                </>
            )
        })
    }

    renderItem(item, index) {

        let maxHeight = this.props.childrenHeight * 1.2;
        let minHeight = this.props.childrenHeight / 1.5;

        let _width = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.childrenWidth, maxHeight]
        });

        let _height = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [minHeight, maxHeight]
        });

        let marginRight = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 4]
        });

        let borderTopLeftRadius = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [(minHeight) / 2, (maxHeight) / 2]
        });

        let borderBottomLeftRadius = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [(minHeight) / 2, (maxHeight) / 2]
        });

        let borderTopRightRadius = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, (maxHeight) / 2]
        });

        let borderBottomRightRadius = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, (maxHeight) / 2]
        });

        let borderWidth = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 2]
        });


        let _style = {
            width: _width,
            height: _height,
            marginRight: marginRight,
            borderTopLeftRadius: borderTopLeftRadius,
            borderBottomLeftRadius: borderBottomLeftRadius,
            borderTopRightRadius: borderTopRightRadius,
            borderBottomRightRadius: borderBottomRightRadius,
            borderWidth: borderWidth,
            borderColor: '#ffffff'
        }

        let labelWidth = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, width * 0.4]
        });

        let labelHeight = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, minHeight]
        });


        let fontSize = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 12]
        });

        let iconSize = this.animPress.interpolate({
            inputRange: [0, 1],
            outputRange: [18, 24]
        });

        return (
            <Animated.View key={index} style={[styles.itemScroll, { height: this.props.childrenHeight }]}>
                <Animated.View style={[{ width: labelWidth, height: labelHeight, marginRight: -10, paddingLeft: 10, flexDirection: 'row', alignItems: 'center', backgroundColor: '#007195', borderTopLeftRadius: borderTopLeftRadius, borderBottomLeftRadius: borderBottomLeftRadius, }]}>
                    <Animated.Text style={[{ fontSize: fontSize, color: '#ffffff' }]}>Scroll down </Animated.Text>
                    <Animated.Image style={[{ marginLeft: 5, width: 16, height: 16, tintColor: '#ffffff' }]} source={require('./assets/icons8-arrow-down-24.png')} />
                </Animated.View>
                <Animated.View style={[styles.itemScrollChildren, _style]}>
                    <Animated.Image style={[{ width: iconSize, height: iconSize, tintColor: '#ffffff' }]} source={require('./assets/icons8-double-down-24.png')} />
                </Animated.View>
            </Animated.View>
        )
    }

    componentWillUnmount() {
        if (this.isScaleRecovery) clearTimeout(this.isScaleRecovery)
    }

}

FastScroll.propTypes = {
    dataSource: PropTypes.array.isRequired,
    parentWidth: PropTypes.number,
    childrenHeight: PropTypes.number.isRequired,
    childrenWidth: PropTypes.number.isRequired,

    sortable: PropTypes.bool,

    onClickItem: PropTypes.func,
    onDragStart: PropTypes.func,
    onDragEnd: PropTypes.func,
    onDataChange: PropTypes.func,
    renderItem: PropTypes.func.isRequired,
    scaleStatus: PropTypes.oneOf(['scale', 'scaleX', 'scaleY']),
    fixedItems: PropTypes.array,
    keyExtractor: PropTypes.func,
    delayLongPress: PropTypes.number,
    isDragFreely: PropTypes.bool,
    onDragging: PropTypes.func,
    maxScale: PropTypes.number,
    minOpacity: PropTypes.number,
    scaleDuration: PropTypes.number,
    slideDuration: PropTypes.number
}

FastScroll.defaultProps = {
    parentWidth: width,
    sortable: true,
    scaleStatus: 'scale',
    fixedItems: [],
    maxScale: 1.1,
    minOpacity: 0.8,
    scaleDuration: 200,
    slideDuration: 300,
    delayLongPress: 10,
}

const styles = StyleSheet.create({
    container: {
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    item: {
        position: 'absolute',
        zIndex: defaultZIndex,
    },
    itemScroll: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    itemScrollChildren: {
        backgroundColor: '#1792C8',
        alignItems: 'center',
        justifyContent: 'center',
    },
    item_text: {
        marginRight: 15,
        color: '#2ecc71'
    }
})