import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, SafeAreaView } from 'react-native'
import FastScroll from './FastScroll'

const { width } = Dimensions.get('window')

const parentWidth = width
const childrenWidth = width * 0.1
const childrenHeight = 48

export default class OneRowsPage extends Component {

    constructor(props) {
        super()

        this.state = {
            data: [
                { title: 'Title of content #1', subtitle: 'See Your Network Activity in the last 30 days', img: 'https://dummyimage.com/250/ff00ff' },
                { title: 'Title of content #2', subtitle: 'See Your Network Activity in the last 30 days', img: 'https://dummyimage.com/250/ff0000' },
                { title: 'Title of content #3', subtitle: 'See Your Network Activity in the last 30 days', img: 'https://dummyimage.com/250/0000ff' },
                { title: 'Title of content #4', subtitle: 'See Your Network Activity in the last 30 days', img: 'https://dummyimage.com/250/ffff00' },
                { title: 'Title of content #5', subtitle: 'See Your Network Activity in the last 30 days', img: 'https://dummyimage.com/250/00ff00' },
                { title: 'Title of content #6', subtitle: 'See Your Network Activity in the last 30 days', img: 'https://dummyimage.com/250/00ffff' },
            ],
            scrollEnabled: true,
        }
    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView
                    ref={(scrollView) => this.scrollView = scrollView}
                    scrollEnabled={this.state.scrollEnabled}
                    contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                    style={styles.container}>
                    <FastScroll
                        dataSource={this.state.data}

                        parentWidth={parentWidth}

                        childrenWidth={childrenWidth}
                        childrenHeight={childrenHeight}

                        scaleStatus={'scaleY'}

                        onDragStart={(startIndex, endIndex) => {
                            this.setState({
                                scrollEnabled: false
                            })
                        }}
                        onDragEnd={(startIndex) => {
                            this.setState({
                                scrollEnabled: true
                            })
                        }}
                        onDataChange={(data) => {
                            if (data.length != this.state.data.length) {
                                this.setState({
                                    data: data
                                })
                            }
                        }}
                        keyExtractor={(item, index) => item.txt} // FlatList作用一样，优化
                        onClickItem={(data, item, index) => {

                        }}
                        renderItem={(item, index) => {
                            return this.renderItem(item, index)
                        }}
                    />
                </ScrollView>
            </SafeAreaView>
        )
    }

    renderItem(item, index) {
        return (
            <View style={styles.item}>
                <View style={styles.item_children}>
                    <Text style={styles.item_text}>{item.txt}</Text>
                </View>
            </View>
        )
    }



}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f0f0f0',
    },
    item: {
        width: '100%',
        height: childrenHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    item_children: {
        width: childrenWidth,
        height: childrenHeight - 4,
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 4,
    },
    item_icon: {
        width: childrenHeight * 0.6,
        height: childrenHeight * 0.6,
        marginLeft: 15,
        resizeMode: 'contain',
    },
    item_text: {
        marginRight: 15,
        color: '#2ecc71'
    }
})